
function validator() {
    const form = document.querySelector(".subscribe__form");
    const emailInput = form.querySelector(".subscribe__input");
    const submitBtn = form.querySelector(".subscribe__form-submit");
  
    function validateEmail(email) {
      const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      return regex.test(email);
    }

    submitBtn.addEventListener("click", (event) => {
      event.preventDefault();
      const email = emailInput.value;
      if (!validateEmail(email)) {
        alert("Please enter a valid email address");
        return;
      }
      alert("Sent to server")
      form.reset();
    });
  }
  
  validator();