import Swiper, { Navigation, Pagination, Autoplay, Scrollbar } from "swiper";
Swiper.use([Navigation, Pagination, Autoplay, Scrollbar]);

function heroSwiper() {
  let swiper = new Swiper(".hero__swiper-swipe", {
    autoplay: {
      delay: 3000,
    },

    breakpoints: {
      375: {
        direction: "horizontal",
        slidesPerView: 1.2,
        centeredSlides: true,
        spaceBetween: 10,
        loop: true,
      },
      480: {
        direction: "horizontal",
        slidesPerView: 1.2,
        centeredSlides: true,
        spaceBetween: 10,
        loop: true,
      },
      768: {
        direction: "horizontal",
        slidesPerView: 2,
        loop: true,
        centeredSlides: true,
        spaceBetween: 20,
      },
      991: {
        direction: "horizontal",
        slidesPerView: 2.5,
        spaceBetween: 20,
        loop: true,
      },
      1440: {
        direction: "horizontal",
        slidesPerView: 2.5,
        spaceBetween: 20,
      },
    },
  });
}

heroSwiper();

function houseSwiperOne() {
  let swiper = new Swiper(".house__swiper-one", {
    slidesPerView: 3.5,
    spaceBetween: 48,
    navigation: {
      nextEl: ".house__swiper-next",
      prevEl: ".house__swiper-prev",
    },

    breakpoints: {
      375: {
        slidesPerView: 1,
      },
      480: {
        slidesPerView: 1,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 10,
        centeredSlides: true,
      },
      991: {
        slidesPerView: 2.5,
        spaceBetween: 10,
        centeredSlides: true,
      },
      1440: {
        slidesPerView: 3.5,
        spaceBetween: 48,
      },
    },
  });
}

houseSwiperOne();

function readySwiper() {
  const readyContainer = document.querySelector(".ready__container");
  let swiper = null;

  readyContainer.addEventListener("mouseenter", () => {
    swiper = new Swiper(".ready__swiper", {
      slidesPerView: 1,
      loop: true,
      autoplay: {
        delay: 5000,
      },
    });
  });

  readyContainer.addEventListener("mouseleave", () => {
    if (swiper !== null) {
      swiper.autoplay.stop();
      swiper = null;
    }
  });
}

readySwiper();

function seeSwiper() {
  let swiper = new Swiper(".see__swiper", {
    slidesPerView: 1.7,
    centeredSlides: true,

    pagination: {
      el: ".swiper-pagination",
      dynamicBullets: true,
    },

    breakpoints: {
      375: {
        slidesPerView: 1,
      },
      480: {
        slidesPerView: 1,
      },
      768: {
        slidesPerView: 1,
        centeredSlides: true,
      },
      991: {
        slidesPerView: 1,
        centeredSlides: true,
      },
      1440: {
        slidesPerView: 1.7,
        centeredSlides: true,
      }
    }
  });
}

seeSwiper();


