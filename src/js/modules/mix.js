import mixitup from "mixitup";

function mix() {
  let containerOne = document.getElementById("mixitupOne");
  if (containerOne) {
    let mixer = mixitup(containerOne);
    mixer.filter(".category-house");
  }
}
mix();

